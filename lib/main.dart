import 'package:flutter/material.dart';
import 'pages/counter_page.dart';
import 'pages/home_page.dart';
import 'pages/content_page.dart';
import 'pages/form_name.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(title: 'Pendalaman Flutter', initialRoute: '/', routes: {
      '/': (context) => HomePage(),
      '/counter': (context) => CounterPage(),
      '/content': (context) =>
          ContentPage(judul: "Konten", kata: "Selamat Datang di Kontent"),
      '/tentang': (context) => ContentPage(
          judul: "Tentang Dev",
          kata: "Aplikasi ini dikembangkan dengan bahasa Dart"),
      '/form-name': (context) => FormName(),
    });
  }
}
