import 'package:flutter/material.dart';

class FormName extends StatefulWidget {
  @override
  _FormName createState() => _FormName();
}

class _FormName extends State<FormName> {
  TextEditingController _controller;

  void initState() {
    super.initState();
    _controller = TextEditingController();
    _controller.text = "";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Form Name")),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Halo Salam Kenal "),
              Text(_controller.text),
              TextField(
                controller: _controller,
                onSubmitted: (text) {
                  setState(() {});
                },
              ),
            ],
          ),
        ));
  }
}
